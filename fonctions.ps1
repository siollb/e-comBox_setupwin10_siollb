# Déclaration des variables

$branche='master'
$version_setup='siollbv2'
$version_appli='siollbv2'


# Déclaration de la fonction pour installer les correctifs

. "$pathscripts\fonction_verifPatch.ps1"
. "$pathscripts\fonction_patch.ps1"


Function valeurConfig
{
    $path = "$env:USERPROFILE\.docker\confEcombox\param.conf"
    If (-not (Test-Path $path))
    {
     curl https://gitlab.com/siollb/e-comBox_setupWin10_siollb/raw/$branche/param.conf -OutFile $path
    }
    
    $file = new-Object System.IO.StreamReader(Get-item –Path "$path")
    $array = @{}    
    while($line = $file.ReadLine())
    {
        $splitLine = $line.Split('=')
        $id = $splitLine[0]
        $value = $splitLine[1]
        $array.Add($id,$value)
    }    
    $file.Close()
    return $array    
} 


Function ajoutInfosLog
{
    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "Informations sur le système utilisé" >> $pathlog\ecombox.log
    Get-ComputerInfo | select OSName, OsArchitecture, OsVersion, WindowsVersion, WindowsCurrentVersion, WindowsBuildLabEx, OsHardwareAbstractionLayer, OsTotalVisibleMemorySize, OsFreePhysicalMemory, OsTotalVirtualMemorySize, OsFreeVirtualMemory, OsInUseVirtualMemory, OsTotalSwapSpaceSize | fl  >> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "Informations sur le CPU et la RAM utilisé par Docker" >> $pathlog\ecombox.log
    docker info 2>$null| Select-String "CPU" >> $pathlog\ecombox.log
    docker info 2>$null | Select-String "Memory" >> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "Informations sur le proxy configuré sur Docker" >> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "Configuration de settings.json" >> $pathlog\ecombox.log
    Get-Content "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" | Select-String "proxyHttp" >> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "Configuration de http_proxy.json" >> $pathlog\ecombox.log
    Get-Content "$env:USERPROFILE\AppData\Roaming\Docker\http_proxy.json" >> $pathlog\ecombox.log
}


Function recupIPhost
{
    # Récupération et mise au bon format de l'adresse IP de l'hôte (l'adresse IP récupérée est celle associée à une passerelle par défaut)
    #$docker_ip_host = (Get-NetIPAddress -InterfaceIndex (Get-NetAdapter -Physical).InterfaceIndex).IPv4Address
    $docker_ip_host = (Get-NetIPAddress -InterfaceIndex (Get-NetIPConfiguration | Foreach IPv4DefaultGateway).ifIndex).IPv4Address  | Select-Object -first 1
    $docker_ip_host = "$docker_ip_host"
    $docker_ip_host = $docker_ip_host.Trim()

    return $docker_ip_host
}


Function recupIPvalides
{
   # Récupération des adresses IP d'une interface physique même si elles ne sont pas associées à une passerelle par défaut
   #$adressesIPvalides = (Get-NetIPAddress -InterfaceIndex (Get-NetAdapter -Physical).InterfaceIndex).IPv4Address | Select-String -NotMatch ^169
   $adressesIPValides = (Get-NetIPAddress -AddressFamily IPv4 -InterfaceIndex (Get-NetAdapter -Physical).InterfaceIndex).IPAddress | Select-String -NotMatch ^169
        
   $adressesIPvalides = "$adressesIPvalides"
   $adressesIPvalides = $adressesIPvalides.Trim()

   return $adressesIPvalides
  
}

Function verifIPvalide {
    $config = valeurConfig
    $docker_ip_host = $config.ADRESSE_IP

    # Récupération des adresses IP d'une interface physique même si elles ne sont pas associées à une passerelle par défaut
    $adressesIPvalides = recupIPvalides

    if ($docker_ip_host -notlike $adressesIPvalides) {
        Write-Output "`nAdresse IP non valide"
        $stop = popupExclamation -titre "Adresse IP non valide" -message "Le système détecte l'adresse IP valide utilisé dans l'e-comBox n'est pas valide.`n`nVeuillez procéder à la réinitialisation de l'environnement et relancer l'application." 
        # Ce n'est pas la peine de continuer
        Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
        Write-host ""
}
}

Function popupInformation 
{ 
   Param([string]$message=’Message...’, [string]$titre=’Titre’) 
 
   $WshShell = New-Object -ComObject wscript.Shell 
   $WshShell.Popup($message, 8, $titre, 64) 
   return "$codeBouton"
}


Function popupExclamation 
{ 
   Param([string]$message=’Message...’, [string]$titre=’Titre’) 
 
   $WshShell = New-Object -ComObject wscript.Shell 
   $WshShell.Popup($message, 0, $titre, 48) 
}


Function popupQuestion 
{ 
   Param([string]$message=’Message...’, [string]$titre=’Titre’) 
 
   $WshShell = New-Object -ComObject wscript.Shell 
   $WshShell.Popup($message, 0, $titre, 36) 
}

Function popupQuestionDefautNon 
{ 
   Param([string]$message=’Message...’, [string]$titre=’Titre’) 
 
   $WshShell = New-Object -ComObject wscript.Shell 
   $WshShell.Popup($message, 0, $titre, 292) 
}

Function popupStop 
{ 
   Param([string]$message=’Message...’, [string]$titre=’Titre’) 
 
   $WshShell = New-Object -ComObject wscript.Shell 
   $WshShell.Popup($message, 0, $titre, 20) 
}

Function testConnectInternet
{
   $adressesIPvalides = recupIPvalides 

   if (($adressesIPvalides -eq $null) -or ! (Test-Connection gitlab.com -ErrorAction SilentlyContinue)) {
 
      $valideIP = popupExclamation -titre "Configuration de l'adresse IP" -message "Le système ne détecte aucune adresse IP permettant l'accès à Internet nécessaire pour pouvoir utiliser e-comBox. `n`nPour autant l'adresse IP $docker_ip_host figure dans le fichier de paramètres mais l'application ne peut être configuré avec celle-ci. `n`nVérifiez votre configuration IP et relancez le programme." >> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log  
      Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
      Write-host ""
      exit
    }
}

Function demarreDocker
{

    $WarningPreference='silentlycontinue'

    #Lancement de Docker en super admin
    #if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -WindowStyle Normal -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }
         Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Arrêt des processus résiduels." >> $pathlog\ecombox.log         

         $process = Get-Process "com.docker.backend" -ErrorAction SilentlyContinue
         if ($process.Count -gt 0)
         {
            Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Le processus com.docker.backend existe et va être stoppé" >> $pathlog\ecombox.log
            Stop-Process -Name "com.docker.backend" -Force  >> $pathlog\ecombox.log        
         }
            else {
                Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Le processus com.docker.backend n'existe pas" >> $pathlog\ecombox.log                                 
            }


         $service = get-service com.docker.service
         if ($service.status -eq "Stopped")
         {
            Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Rien à faire car Docker est arrêté." >> $pathlog\ecombox.log             
         }
            else
            {
               Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Le service Docker va être stoppé." >> $pathlog\ecombox.log
               net stop com.docker.service >> $pathlog\ecombox.log              
            }


           foreach($svc in (Get-Service | Where-Object {$_.name -ilike "*docker*" -and $_.Status -ieq "Running"}))
           {
              $svc | Stop-Service -ErrorAction Continue -Confirm:$false -Force
              $svc.WaitForStatus('Stopped','00:00:20')               
           }

           Get-Process | Where-Object {$_.Name -ilike "*docker*"} | Stop-Process -ErrorAction Continue -Confirm:$false -Force            

          foreach($svc in (Get-Service | Where-Object {$_.name -ilike "*docker*" -and $_.Status -ieq "Stopped"} ))
          {
             $svc | Start-Service 
             $svc.WaitForStatus('Running','00:00:20')              
          }

          Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Démarrage de Docker"
          & "C:\Program Files\Docker\Docker\Docker Desktop.exe"
          $startTimeout = [DateTime]::Now.AddSeconds(90)
          $timeoutHit = $true
           
          while ((Get-Date) -le $startTimeout)
          {

          Start-Sleep -Seconds 10                     

          try
         {
            $info = (docker info 2>$null)
            Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `tDocker info executed. Is Error?: $($info -ilike "*error*"). Result was: $info" >> $pathlog\ecombox.log            
            if ($info -ilike "*error*")
            {
               Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `tDocker info had an error. throwing..." >> $pathlog\ecombox.log
               throw "Error running info command $info"                
            }
            $timeoutHit = $false
            break
         }
         catch 
         {

             if (($_ -ilike "*error during connect*") -or ($_ -ilike "*errors pretty printing info*")  -or ($_ -ilike "*Error running info command*"))
             {
                 Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) -`t Docker Desktop n'a pas encore complètement démarré, il faut attendre." >> $pathlog\ecombox.log
                 Write-host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) -`t Docker Desktop n'a pas encore complètement démarré, il faut attendre."
             }
             else
            {
                write-host ""
                Write-host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Docker a démarré. Le processus peut continuer."
                #write-host "Unexpected Error: `n $_" 2>$null
                #Write-Output "Unexpected Error: `n $_" 2>$null >> $pathlog\ecombox.log
                return
            }
         }
         $ErrorActionPreference = 'Stop'
     }
     if ($timeoutHit -eq $true)
     {
         throw "Délai d'attente en attente du démarrage de docker"
     }
        
     Write-host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Docker a démarré. Le processus peut continuer."
     Write-host ""
     Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Docker a démarré. Le processus peut continuer." >> $pathlog\ecombox.log
     Write-Output "" >> $pathlog\ecombox.log 
}


Function verifDocker
{
    # Vérification que Docker fonctionne correctement sinon ce n'est pas la peine de continuer

    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Vérification de Docker." >> $pathlog\ecombox.log
    Write-host ""
    Write-host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Vérification de l'état de Docker"
    Write-host ""

    docker-compose version *>> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
    docker info 2>$null *>> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log

    $error.clear()
    $info_docker = (docker info 2>$null)

    if (($info_docker -ilike "*error*") -or ($error -ilike "*error*") -or ($info_docker -ilike "*pas reconnu comme nom*"))
      {      
         Write-Output "Docker n'est pas démarré. Le processus doit démarrer Docker avant de continuer..." >> $pathlog\ecombox.log 
         Write-Output "" >> $pathlog\ecombox.log
         Write-host "Le processus doit démarrer Docker avant de continuer..."
         write-host ""
         demarreDocker    
      }
      else {
            Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Docker est démarré. Le processus peut continuer..." >> $pathlog\ecombox.log 
            Write-Output "" >> $pathlog\ecombox.log
            Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Docker est démarré. Le processus peut continuer..." 
            Write-Host ""          
            }

}

Function verifPortainerApresVerifDocker
{
    # Vérification que Portainer fonctionne correctement sinon on redémarre Docker
    if (Test-Path "$env:USERPROFILE\e-comBox_portainer_siollb") {

    if ((docker ps -a -f "status=created" | Select-String e-combox) -or (docker ps -a -f "status=created" | Select-String portainer) -or (docker ps -a -f "status=exited" | Select-String e-combox) -or (docker ps -a -f "status=exited" | Select-String portainer)) 
       { 
         Write-Output "Portainer a mal redémarré. Le processus doit re-démarrer Docker avant de continuer..." >> $pathlog\ecombox.log 
         Write-Output "" >> $pathlog\ecombox.log
         Write-host "Le processus doit re-démarrer Docker avant de continuer..."
         write-host ""
         demarreDocker    
       }
       else {
            Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Portainer est correctement lancé. Le processus peut continuer..." >> $pathlog\ecombox.log 
            Write-Output "" >> $pathlog\ecombox.log
            #Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Portainer est correctement lancé. Le processus peut continuer..." 
            #Write-Host ""          
            }
     }
      else {
           Write-Output "Dossier Portainer inexistant - Portainer n'est pas encore installé, ceci est normal en phase d'initialisation." >> $pathlog\ecombox.log 
           Write-Output "" >> $pathlog\ecombox.log
           #Write-host "Dossier Portainer inexistant - Portainer n'est pas encore installé, ceci est normal en phase d'initialisation"
           #write-host ""
     }
}

Function wslConfig
{
    # Récupération de la mémoire max du PC
    $memoire_pc = (Get-ComputerInfo).CsPhyicallyInstalledMemory/1MB

    # Récupération de la mémoire max utilisée par Docker/WSL2 via le fichier .wslconfig
    $ligne_mem = Get-Content $env:USERPROFILE\.wslconfig | Select-String memory
    $memoire_wsl2 = ("$ligne_mem".Split('='))[1]
    
    # Configuration éventuelle de la mémoire vive utilisée par WSL2
    $reponse = popupQuestionDefautNon -titre "Configuration de la mémoire vive (RAM) utilisée" -message "Vous disposez au total de $memoire_pc Gigabit et l'e-comBox utilise $memoire_wsl2.`n`nVous pouvez changer ce paramètre sachant que Windows a besoin d'au moins 4GB de libre pour fonctionner correctement.`n`nDésirez-vous modifier la quantité de RAM dédiée à e-comBox ?" -Foreground Yellow 
 
    if ($reponse -eq 6) 
     {
        $RAM=Read-Host "`n`nSaisissez la quantité de mémoire vive (RAM) que vous désirez allouer à l'e-comBox en saisissant également l'unité (par exemple 8GB ou 512MB)."
             
        # Affectation de cette mémoire au fichier .wslconfig
        (Get-Content -Path $env:USERPROFILE\.wslconfig) | ForEach-Object {$_ -replace "$ligne_mem", "memory=$RAM"} | Set-Content -Path $env:USERPROFILE\.wslconfig
                
        # Redémarrage de wsl
        $info = popupInformation -titre "Redémarrage de Docker" -message "Docker va automatiquement redémarrer pour prendre en compte les modifications."
        wsl --shutdown
        demarreDocker
     }
}

Function retourneAdresseProxy {

    $reg = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"

    $settings = Get-ItemProperty -Path $reg

    if ($settings.ProxyEnable -eq 1) {
       $adresseProxy = $settings.ProxyServer
       if ($adresseProxy -ilike "*=*")
           {
            $adresseProxy = $adresseProxy -replace "=","://" -split(';') | Select-Object -First 1
            $adresseProxy = $adresseProxy.TrimStart("http://")
           }

           else
          {
            #$adresseProxy = "http://" + $adresseProxy
            $adresseProxy = $adresseProxy
        }
    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "le proxy est activé et configuré à $adresseProxy" >> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
       
    }
    else {
       $adresseProxy = ""
    }

   # Mise à jour du fichier de paramètres sans le HTTP
   $ligne = Get-Content $pathconf\param.conf | Select-String ADRESSE_PROXY
   (Get-Content -Path $pathconf\param.conf) | ForEach-Object {$_ -replace "$ligne", "ADRESSE_PROXY=$adresseProxy"} | Set-Content -Path $pathconf\param.conf
  
   return $adresseProxy
  
}


Function retourneNoProxy {

  $reg = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"

  $settings = Get-ItemProperty -Path $reg

  if ($settings.ProxyEnable -eq 1) {
    $noProxy = $settings.ProxyOverride

    if ($noProxy)
       { 
             $noProxy = $noProxy.Replace(';',',')
       }
       else
       {     
             $noProxy = "localhost"
       }

    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "le proxy est activé et le noProxy est $noProxy" >> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
  }
   else {
    $noProxy = ""
  }

   # Mise à jour du fichier de paramètres
   $ligne = Get-Content $pathconf\param.conf | Select-String NO_PROXY
   (Get-Content -Path $pathconf\param.conf) | ForEach-Object {$_ -replace "$ligne", "NO_PROXY=$noProxy"} | Set-Content -Path $pathconf\param.conf
  
  return $noProxy
}


Function verifNoProxy {
    
    $config = valeurConfig
    $noProxy = $config.NO_PROXY
    $adresse_ip = $config.ADRESSE_IP    
    $cidr = (Get-NetIPAddress -IPAddress $adresse_ip).PrefixLength

    Write-Output "La configuration IP du poste est $adresse_ip/$cidr" >> $pathlog\ecombox.log
   
    # Conversion du masque cidr en binaire
    [Int[]]$array = (1..32) 
    for($i=0;$i -lt $array.length;$i++){ 
       if($array[$i] -gt $cidr){$array[$i]="0"} else{$array[$i]="1"} 
    } 
      $cidr_binaire =$array -join ""       
      
    # Conversion du masque binaire en décimal    
    $mask=([System.Net.IPAddress]"$([System.Convert]::ToInt64($cidr_binaire,2))").IPAddressToString
   

    # Détermination de l'adresse réseau
    $ip = [ipaddress]$adresse_ip
    $mask = [ipaddress]$mask
    $reseau = ([ipaddress]($ip.Address -band $mask.Address)).IPAddressToString 2>>$null
    
    Write-Output "Le réseau est $reseau" >> $pathlog\ecombox.log
    
    # Ajout du caractère générique "*"
    if ($cidr -le 8) {
        $reseauNoProxy = $reseau.TrimEnd("0")
        $reseauNoProxy = $reseauNoProxy.TrimEnd(".")
        $reseauNoProxy = $reseauNoProxy.TrimEnd("0")
        $reseauNoProxy = $reseauNoProxy.TrimEnd(".")
        $reseauNoProxy = $reseauNoProxy.TrimEnd("0")        
    } 
        else {
            if ($cidr -ge 24) {
               $reseauNoProxy = $reseau.TrimEnd("0")          
            }
            else {
                $reseauNoProxy = $reseau.TrimEnd("0")
                $reseauNoProxy = $reseauNoProxy.TrimEnd(".")
                $reseauNoProxy = $reseauNoProxy.TrimEnd("0")                
            }
        }

    if ($cidr -le 24) {
        $reseauNoProxy = $reseauNoProxy+"*"        
    } 

    Write-Output "Le réseau à ajouter au noProxy est $reseauNoProxy" >> $pathlog\ecombox.log
    return $reseauNoProxy       
}


Function configProxyGit 
{ 

 $config = valeurConfig
 $adresseProxy = $config.ADRESSE_PROXY
 $noProxy = $config.NO_PROXY

 Write-Output "" >> $pathlog\ecombox.log
 Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Configuration du Proxy pour GIT" >> $pathlog\ecombox.log 
 Write-Output "" >> $pathlog\ecombox.log


 Set-Location -Path $env:USERPROFILE

 if ($adresseProxy) {      
    # Configuration de Git
    $adresseProxy = "http://" + $config.ADRESSE_PROXY
    git config --global http.proxy $adresseProxy *>> $pathlog\ecombox.log
    }

    else {
      # Configuration de Git
      git config --global --unset http.proxy *>> $pathlog\ecombox.log
      }
    
}


Function configProxyDocker
{
 $adProxy=retourneAdresseProxy
 $byPass=retourneNoProxy

 $config = valeurConfig
 $adresseProxy = $config.ADRESSE_PROXY
 $noProxy = $config.NO_PROXY

 Write-Output "" >> $pathlog\ecombox.log
 Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Configuration du proxy pour Docker" >> $pathlog\ecombox.log
 Write-Output "" >> $pathlog\ecombox.log

 
 Set-Location -Path $env:USERPROFILE\.docker

 if ($adresseProxy) {

     new-item "config.json" –type file -force *>> $pathlog\ecombox.log     

     If ($? -eq 0) {
        $allProcesses = Get-Process
        foreach ($process in $allProcesses) { 
             $process.Modules | where {$_.FileName -eq "$env:USERPROFILE\.docker\config.json"} | Stop-Process -Force -ErrorAction SilentlyContinue
        }
        Remove-Item "config.json" *>> $pathlog\ecombox.log
        New-Item -Path "config.json" -ItemType file -force *>> $pathlog\ecombox.log
       }

$adresseProxy = "http://" + $config.ADRESSE_PROXY

@"
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "$adresseProxy",
     "httpsProxy": "$adresseProxy",
     "noProxy": "$noProxy"
   }
 }
}
"@ > config.json

     Set-Content config.json -Encoding ASCII -Value (Get-Content config.json) *>> $pathlog\ecombox.log

     Write-Output ""  >> $pathlog\ecombox.log
     Write-Output "Le fichier config.json a été créé et complété."  >> $pathlog\ecombox.log
     Write-Output ""  >> $pathlog\ecombox.log
     }
     else {
         remove-item "config.json" *>> $pathlog\ecombox.log
         Write-Output ""  >> $pathlog\ecombox.log
         Write-Output "Le proxy est désactivé et le fichier config.json a été supprimé"  >> $pathlog\ecombox.log
         Write-Output ""  >> $pathlog\ecombox.log         
      }
}

Function configProxyDockerAvecPopup
{

 $adProxy=retourneAdresseProxy
 $byPass=retourneNoProxy

 $config = valeurConfig
 $adresseProxy = $config.ADRESSE_PROXY
 $noProxy = $config.NO_PROXY 

 Write-Output "" >> $pathlog\ecombox.log
 Write-Output "Popup pour la vérification de la configuration du Proxy sur Docker" >> $pathlog\ecombox.log
 Write-Output "" >> $pathlog\ecombox.log

 Set-Location -Path $env:USERPROFILE\.docker

 if ($adresseProxy) {
     $reseauNoProxy = verifNoProxy
     popupExclamation -titre "Configuration du proxy" -message "Le système a détecté que vous utilisez un proxy pour vous connecter à Internet, vérifiez que ce dernier soit correctement configuré au niveau de Docker avec les paramètres suivants :`n`nAdresse IP du proxy (avec le port utilisé) pour HTTP et HTTPS : $adresseProxy `nBy Pass : $noProxy `n`n`Vérifiez que la valeur $reseauNoProxy figure bien dans les valeurs à exclure du proxy au niveau du champs By Pass. Si ce n'est pas le cas, il est nécessaire de l'ajouter sur votre système et sur Docker.`n`nSi vous venez de procéder à la configuration, il faut attendre que Docker ait redémarré avant de cliquer sur OK pour continuer."
     
     new-item "config.json" –type file -force *>> $pathlog\ecombox.log

     If ($? -eq 0) {
        $allProcesses = Get-Process
        foreach ($process in $allProcesses) { 
             $process.Modules | where {$_.FileName -eq "$env:USERPROFILE\.docker\config.json"} | Stop-Process -Force -ErrorAction SilentlyContinue
        }
        Remove-Item "config.json" *>> $pathlog\ecombox.log
        New-Item -Path "config.json" -ItemType file -force *>> $pathlog\ecombox.log
       }
$adresseProxy = "http://" + $config.ADRESSE_PROXY
@"
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "$adresseProxy",
     "httpsProxy": "$adresseProxy",
     "noProxy": "$noProxy"
   }
 }
}
"@ > config.json

     Set-Content config.json -Encoding ASCII -Value (Get-Content config.json) *>> $pathlog\ecombox.log

     Write-Output ""  >> $pathlog\ecombox.log
     Write-Output "Le fichier config.json a été créé et complété."  >> $pathlog\ecombox.log
     Write-Output ""  >> $pathlog\ecombox.log
     # Fin ajout

}
     else {
         #Ajout
         remove-item "config.json" *>> $pathlog\ecombox.log
         # Fin ajout
         Write-Output ""  >> $pathlog\ecombox.log
         Write-Output "Le proxy est désactivé et le fichier config.json a été supprimé"  >> $pathlog\ecombox.log
         Write-Output ""  >> $pathlog\ecombox.log
         
         popupExclamation -titre "Configuration du proxy" -message "Le système a détecté que vous n'utilisez pas de proxy pour vous connecter à Internet, vérifiez que cette fonctionnalité soit bien désactivée sur Docker. `n`nSi vous venez de procéder à la désactivation, il faut attendre que Docker ait redémarré avant de cliquer sur OK pour continuer."    
      }

}

function verifCoherenceProxy {

    $http_mode=Get-Content "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" | Select-String "proxyHttpMode"
    $http_mode=$http_mode.Line.Split(":")[1].TrimEnd(",").trim()


    # Proxy désactivé sur Docker Desktop mais encore pris en compte par Docker (bug de Docker)

    $http_proxy=docker info 2>$null | Select-String "HTTP Proxy"
        
    if ($http_mode -eq "false" -and ($http_proxy -isnot $null -or $http_proxy -ne "")) {
   
       write-Output "Le Proxy est désactivé sur Docker Desktop mais Docker continue à le prendre en compte."
       $stop = popupExclamation -titre "Modification du proxy" -message "Le système constate que le proxy est désactivé sur Docker Desktop mais qu'il est encore pris en compte par l'application.`n`nLa modification nécessaire va être réalisée et Docker sera redémarré." 

       Set-Content -Path "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" -Value $content
       $content = Get-Content "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" | foreach { $_ -replace "`"overrideProxyHttp`":.+","`"overrideProxyHttp`": `"`"," } 
       Set-Content -Path "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" -Value $content
       $content = Get-Content "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" | foreach { $_ -replace "`"overrideProxyHttps`":.+","`"overrideProxyHttps`": `"`"," } 
       Set-Content -Path "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" -Value $content
       $content = Get-Content "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" | foreach { $_ -replace "`"overrideProxyExclude`":.+","`"overrideProxyExclude`": `"`"," } 
       Set-Content -Path "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" -Value $content
   
       demarreDocker

       write-Output "Le Proxy a été correctement désactivé."

     }

    #vérif si le proxy doit être activé ou désactivé sur Docker Desktop
    $config = valeurConfig
    $docker_ip_proxy = $config.ADRESSE_PROXY

    # Proxy dans param.conf non configuré sur Docker Desktop
    if ($http_mode -eq "false" -and ($docker_ip_proxy -isnot $null -or $docker_ip_proxy -ne "")) {
       Write-Output "`nIl y a un proxy sur le système qui n'est pas configuré sur Docker Desktop."
       $stop = popupExclamation -titre "Proxy à configurer sur Docker Desktop" -message "Le système détecte un proxy sur le système qui n'est pas configuré sur Docker Desktop.`n`nVeuillez procéder à la réinitialisation de l'environnement et relancer l'application." 
       # Ce n'est pas la peine de continuer
       Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
       Write-host ""
    }

    # Proxy configuré sur Docker Desktop non configuré dans param.conf
    if (($http_mode -eq "true") -and ($docker_ip_proxy -is $null -or $docker_ip_proxy -eq "")) {
        Write-Output "`nIl y a un proxy configuré sur Docker Desktop non configuré sur l'application."
        $stop = popupExclamation -titre "Proxy configuré sur Docker Desktop" -message "Le système détecte que le proxy est activé sur Docker Desktop mais n'est pas pris en compte par l'application.`n`nVeuillez procéder à la réinitialisation de l'environnement ou désactiver le proxy sur Docker Desktop puis  relancer l'application." 
        # Ce n'est pas la peine de continuer
        Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
        Write-host ""
    }

    # Proxy système non configuré dans param.conf
    if ($docker_ip_proxy -eq "" -or $docker_ip_proxy -is $null) {
        $adresse_proxy = retourneAdresseProxy
        if ($adresse_proxy -ne "") {
            Write-Output "`nIl y a un proxy sur le système qui n'est pas configuré pour e-comBox"
            $stop = popupExclamation -titre "Proxy à configurer sur e-comBox" -message "Le système détecte un proxy sur le système qui n'est pas configuré pour e-comBox.`n`nVeuillez procéder à la réinitialisation de l'environnement et relancer l'application." 
            # Ce n'est pas la peine de continuer
            Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
            Write-host ""
        }
    }

}

Function recupPortainer 
{ 
Write-Output "" >> $pathlog\ecombox.log
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Récupération de portainer" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

$Path="$env:USERPROFILE\e-comBox_portainer_siollb\"
$TestPath=Test-Path $Path

If ($TestPath -eq $False) {
    # Récupération de Portainer sur Git
    Write-Output ""  >> $pathlog\ecombox.log
    Write-Output "Téléchargement de Portainer" >> $pathlog\ecombox.log
    write-host ""
    write-host "Téléchargement de Portainer"
    Set-Location -Path $env:USERPROFILE
    git clone -b $branche https://gitlab.com/siollb/e-comBox_portainer_siollb *>> $pathlog\ecombox.log
    }
    else {
      # Suppression de portainer et installation d'un éventuel nouveau Portainer
      Write-Output ""  >> $pathlog\ecombox.log
      Write-Output "    --> Portainer est installé, il faut le supprimer pour le réinstaller."  >> $pathlog\ecombox.log
      Write-Output ""  >> $pathlog\ecombox.log
      write-host ""
      write-host "Téléchargement de Portainer"
      Set-Location -Path $Path    
      docker-compose down *>> $pathlog\ecombox.log

      Set-Location -Path $env:USERPROFILE   
      Remove-Item "$env:USERPROFILE\e-comBox_portainer_siollb" -Recurse -Force *>> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log
      Write-Output "Téléchargement de Portainer" >> $pathlog\ecombox.log
      git clone -b $branche https://gitlab.com/siollb/e-comBox_portainer_siollb *>> $pathlog\ecombox.log 
    }
   
If (Test-Path "$env:USERPROFILE\e-comBox_portainer_siollb") {
    write-host ""
    write-host "Succès... Portainer a été téléchargé."
    write-host ""
    write-Output "" >> $pathlog\ecombox.log
    write-Output "Succès... Portainer a été téléchargé." >> $pathlog\ecombox.log
    write-Output "" >> $pathlog\ecombox.log

}
    else {
       write-host ""
       write-host "Portainer n'a pas pu être téléchargé. Il y a un problème avec la commande git. Le processus va s'arrêter car ce n'est pas la peine de continuer. Consultez les logs pour avoir les détails de l'erreur."
       Write-Host ""
       write-Output "" >> $pathlog\ecombox.log
       write-Output "Portainer n'a pas pu être téléchargé. Il y a un problème avec la commande git" >> $pathlog\ecombox.log
       write-Output "" >> $pathlog\ecombox.log
       # Ce n'est pas la peine de continuer
       Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
       Write-host ""
       exit
    }      
}


Function configPortainer 
{
$config = valeurConfig
Set-Location -Path $env:USERPROFILE\e-comBox_portainer_siollb\ *>> $pathlog\ecombox.log

Write-Output "" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Configuration de Portainer" >> $pathlog\ecombox.log
Write-Host ""
Write-Host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Configuration de Portainer" 

# Récupération de l'IP du poste
$docker_ip_host = $config.ADRESSE_IP

# Récupération du port (8880 par défaut) dans le fichier paramètre
$portainer_port = $config.PORT_PORTAINER

# Mise à jour de l'adresse IP et du port pour Portainer dans le fichier ".env"
New-Item -Name ".env" -ItemType file -force *>> $pathlog\ecombox.log

If ($? -eq 0) {
  $allProcesses = Get-Process
  foreach ($process in $allProcesses) { 
    $process.Modules | where {$_.FileName -eq "$env:USERPROFILE\e-comBox_portainer_siollb\.env"} | Stop-Process -Force -ErrorAction SilentlyContinue
  }

Remove-Item ".env"  *>> $pathlog\ecombox.log
New-Item -Path ".env" -ItemType file -force  *>> $pathlog\ecombox.log
}

Write-Output ""  >> $pathlog\ecombox.log
Write-Output "le fichier .env a été créé"  >> $pathlog\ecombox.log
Write-Output ""  >> $pathlog\ecombox.log

@"
URL_UTILE=$docker_ip_host
PORT=$portainer_port
"@ > .env

Set-Content .env -Encoding ASCII -Value (Get-Content .env)
Write-Output "" >> $pathlog\ecombox.log
Write-Output "Le fichier .env a été mis à jour avec l'adresse IP $docker_ip_host et le port $portainer_port"  >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

}


Function configPortainerAvecPopup 
{
#$config = valeurConfig
}

Function startPortainer 
{ 
# Lancement de Portainer - URL : http://localhost:8880/portainer)
Set-Location -Path $env:USERPROFILE\e-comBox_portainer_siollb\

Write-Output "" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Lancement de Portainer" >> $pathlog\ecombox.log
Write-Host ""
Write-Host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Lancement de Portainer" 

If (docker ps -a | Select-String "name=portainer-app") {
   docker rm -f portainer-app *>> $pathlog\ecombox.log
   }
If (docker ps -a | Select-String "name=portainer-proxy") {
   docker rm -f portainer-proxy *>> $pathlog\ecombox.log
   }
If (docker ps -a --format '{{.Names}}' |  Select-String portainer-proxy) {
   docker rm -f $(docker ps -a --format '{{.Names}}' |  Select-String portainer-proxy)  *>> $pathlog\ecombox.log
   }
docker-compose up -d *>> $pathlog\ecombox.log

}


Function verifPortainer {
if ((docker ps |  Select-String portainer-proxy)) {
    write-host ""
    write-host "Portainer est UP, on peut continuer."
    write-host ""
    write-Output "" >> $pathlog\ecombox.log
    write-Output "Portainer est UP, on peut continuer" >> $pathlog\ecombox.log
    write-Output "" >> $pathlog\ecombox.log
    }
      else {
            write-host ""
            write-host "Toutes les tentatives pour démarrer Portainer ont échoué, essayez de stopper et de démarrer e-comBox puis de réinitialiser l'environnement"
            Write-Host ""
            Write-Output "" >> $pathlog\ecombox.log
            Write-Output "Toutes les tentatives pour démarrer Portainer ont échoué" >> $pathlog\ecombox.log
            # Ce n'est pas la peine de continuer
            Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
            Write-host ""
            exit
            }    
}

Function recupReverseProxy 
{ 

Write-Output "" >> $pathlog\ecombox.log
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Récupération du reverse proxy" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

$Path="$env:USERPROFILE\e-comBox_reverseproxy_siollb\"
$TestPath=Test-Path $Path

If ($TestPath -eq $False) {
    # Récupération du reverse proxy sur Git
    Write-Output ""  >> $pathlog\ecombox.log
    Write-Output "Téléchargement du reverse proxy" >> $pathlog\ecombox.log
    write-host ""
    write-host "Téléchargement du reverse proxy"
    Set-Location -Path $env:USERPROFILE
    git clone -b $branche https://gitlab.com/siollb/e-comBox_reverseproxy_siollb.git *>> $pathlog\ecombox.log
    }
    else {
      # Suppression du reverse proxy et installation d'un éventuel nouveau reverse proxy
      Write-Output ""  >> $pathlog\ecombox.log
      Write-Output "    --> Le reverse proxy existe et va être remplacé."  >> $pathlog\ecombox.log
      Write-Output ""  >> $pathlog\ecombox.log
      write-host ""

      Set-Location -Path $env:USERPROFILE\e-comBox_reverseproxy_siollb\ *>> $pathlog\ecombox.log 
      docker-compose down *>> $pathlog\ecombox.log
      docker image rm -f reseaucerta/docker-gen:2.1 *>> $pathlog\ecombox.log
      docker volume rm e-combox_reverseproxy_nginx-html *>> $pathlog\ecombox.log
      docker volume rm e-combox_reverseproxy_nginx-docker-gen-templates *>> $pathlog\ecombox.log

      Set-Location -Path $env:USERPROFILE   
      Remove-Item "$env:USERPROFILE\e-comBox_reverseproxy_siollb" -Recurse -Force *>> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log
      Write-Output "Téléchargement du reverse proxy" >> $pathlog\ecombox.log
      write-host "Téléchargement du reverse proxy"  
      git clone -b $branche https://gitlab.com/siollb/e-comBox_reverseproxy_siollb.git *>> $pathlog\ecombox.log 
    }
   
If (Test-Path "$env:USERPROFILE\e-comBox_reverseproxy_siollb") {
    write-host ""
    write-host "Succès... Le reverse proxy a été téléchargé."
    write-host ""
    write-Output "" >> $pathlog\ecombox.log
    write-Output "Succès... Le reverse proxy a été téléchargé." >> $pathlog\ecombox.log
    write-Output "" >> $pathlog\ecombox.log

}
    else {
       write-host ""
       write-host "Le reverse proxy n'a pas pu être téléchargé. Il y a un problème avec la commande git. Le processus va s'arrêter car ce n'est pas la peine de continuer. Consultez les logs pour avoir les détails de l'erreur."
       Write-Host ""
       write-Output "" >> $pathlog\ecombox.log
       write-Output "Le reverse proxy n'a pas pu être téléchargé. Il y a un problème avec la commande git" >> $pathlog\ecombox.log
       write-Output "" >> $pathlog\ecombox.log
       # Ce n'est pas la peine de continuer
       Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
       Write-host ""
       exit
    }  
}

Function configReverseProxy 
{
$config = valeurConfig
Set-Location -Path $env:USERPROFILE\e-comBox_reverseproxy_siollb\ *>> $pathlog\ecombox.log

# Récupération de l'IP du poste dans le fichier paramètre
$docker_ip_host = $config.ADRESSE_IP

# Récupération du port (8888 par défaut) dans le fichier paramètre
$rp_port = $config.PORT_RP

# Mise à jour de l'adresse IP et du port pour le reverse proxy dans le fichier ".env"
New-Item -Name ".env" -ItemType file -force *>> $pathlog\ecombox.log

If ($? -eq 0) {
  $allProcesses = Get-Process
  foreach ($process in $allProcesses) { 
    $process.Modules | where {$_.FileName -eq "$env:USERPROFILE\e-comBox_reverseproxy_siollb\.env"} | Stop-Process -Force -ErrorAction SilentlyContinue
  }

Remove-Item ".env"  *>> $pathlog\ecombox.log
New-Item -Path ".env" -ItemType file -force  *>> $pathlog\ecombox.log
}

Write-Output ""  >> $pathlog\ecombox.log
Write-Output "Le fichier .env a été créé."  >> $pathlog\ecombox.log
Write-Output ""  >> $pathlog\ecombox.log

@"
URL_UTILE=$docker_ip_host
NGINX_PORT=$rp_port
"@ > .env

Set-Content .env -Encoding ASCII -Value (Get-Content .env)
Write-Output "" >> $pathlog\ecombox.log
Write-Output "Le fichier .env a été mis à jour avec l'adresse IP $docker_ip_host et le port $rp_port"  >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

}


Function startReverseProxy 
{ 
# Lancement du Reverse Proxy

Set-Location -Path $env:USERPROFILE\e-comBox_reverseproxy_siollb\ *>> $pathlog\ecombox.log

Write-Output "" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Lancement du Reverse Proxy" >> $pathlog\ecombox.log
Write-Host ""
Write-Host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Lancement du Reverse Proxy" 

Set-Location -Path $env:USERPROFILE\e-comBox_reverseproxy_siollb\ *>> $pathlog\ecombox.log
docker-compose up -d *>> $pathlog\ecombox.log

}


Function verifReverseProxy {
if ((docker ps |  Select-String nginx) -and (docker ps | Select-String reseaucerta/docker-gen)) {
    write-host ""
    write-host "Le Reverse Proxy est UP, on peut continuer."
    write-host ""
    write-Output "" >> $pathlog\ecombox.log
    write-Output "Le Reverse Proxy est UP, on peut continuer." >> $pathlog\ecombox.log
    write-Output "" >> $pathlog\ecombox.log
    }
      else {
            if (docker ps |  Select-String nginx) {Write-Output "Le conteneur nginx n'est pas démarré."}
            if (docker ps |  Select-String reseaucerta/docker-gen) {Write-Output "Le conteneur docker-gen n'est pas démarré."}
            write-host ""
            write-host "Toutes les tentatives pour démarrer Reverse Proxy ont échoué, essayez de stopper et de démarrer e-comBox puis de réinitialiser l'environnement"
            Write-Host ""
            Write-Output "" >> $pathlog\ecombox.log
            Write-Output "Toutes les tentatives pour démarrer Reverse Proxy ont échoué" >> $pathlog\ecombox.log
            # Ce n'est pas la peine de continuer
            Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
            Write-host ""
            exit
            }    
}

Function deleteApplication
{
 Write-Output "" >> $pathlog\ecombox.log
  Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
  Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Suppression de l'application" >> $pathlog\ecombox.log
  Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
  Write-Output "" >> $pathlog\ecombox.log
 
   #Suppression d'une éventuelle application
   if ((docker ps -a |  Select-String e-combox)) {
     Write-Output "" >> $pathlog\ecombox.log
     Write-Output "Suppression d'e-combox avec son volume :" >> $pathlog\ecombox.log
     docker rm -f e-combox *>> $pathlog\ecombox.log
     docker volume rm ecombox_data *>> $pathlog\ecombox.log     
   }
    else {
       Write-Output "" >> $pathlog\ecombox.log
       Write-Output "Pas d'application e-combox trouvée." >> $pathlog\ecombox.log      
     } 
}

Function startApplication
{
  $config = valeurConfig
  $port_externe_ecb = $config.PORT_ECB
  $ports_ecb = -join("$port_externe_ecb",":","80")
  
  Write-Output "" >> $pathlog\ecombox.log
  Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
  Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Téléchargement et lancement de l'application" >> $pathlog\ecombox.log
  Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
  Write-Output "" >> $pathlog\ecombox.log

  # Téléchargement d'e-comBox
  Write-Output "" >> $pathlog\ecombox.log
  Write-Output "Téléchargement d'e-comBox :" >> $pathlog\ecombox.log
  Write-Host ""
  Write-Host "Téléchargement d'e-comBox."      
  docker pull reseaucerta/e-combox:$version_appli *>> $pathlog\ecombox.log

  # Lancement d'e-comBox
  Write-Output "Pré-lancement d'e-comBox :" >> $pathlog\ecombox.log
  Write-Host ""
  Write-Host "Pré-lancement d'e-comBox."      
  docker pull reseaucerta/e-combox:$version_appli *>> $pathlog\ecombox.log 
  docker run -dit --name e-combox -v ecombox_data:/usr/local/apache2/htdocs/ -v ecombox_config:/etc/ecombox-conf --restart always -p $ports_ecb --network bridge_e-combox reseaucerta/e-combox:$version_appli *>> $pathlog\ecombox.log
  
  Write-Host ""  
  Write-host "Téléchargement et pré-lancement d'e-comBox fait FAIT."
}

Function configApplication
{
     Write-Output "" >> $pathlog\ecombox.log
     Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
     Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Configuration de l'application" >> $pathlog\ecombox.log
     Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
     Write-Output "" >> $pathlog\ecombox.log

     Write-Host ""
     Write-Host "Configuration d'e-comBox."  

     # Récupération de l'URL d'accès à portainer
     $ancienneURL =  docker exec e-combox bash -c 'grep -ni \"var endpoint\" htdocs/main.js | cut -d\"/\" -f3'

     # Remplacement par la bonne URL
     $env = Get-Content $env:USERPROFILE\e-comBox_portainer_siollb\.env
     $ip_host = $env[0].split("=")[1]
     $port_portainer = $env[1].split("=")[1]
     
     $nouvelleURL = -join("$ip_host",":","$port_portainer")

     if ($ancienneURL -ne $nouvelleURL) {
        #Write-Host "L'ancienne URL $ancienneURL sera remplacée par la nouvelle $nouvelleURL."
        Write-Output "" >> $pathlog\ecombox.log
        Write-Output "L'ancienne URL $ancienneURL sera remplacée par la nouvelle $nouvelleURL." >> $pathlog\ecombox.log
        docker exec e-combox sed -i "s/$ancienneURL/$nouvelleURL/g" htdocs/main.js
        docker exec e-combox sed -i "s/$ancienneURL/$nouvelleURL/g" htdocs/main.js.map
        docker exec e-combox sed -i "s/$ancienneURL/$nouvelleURL/g" htdocs/app-ecombox-ecombox-module.js
        docker exec e-combox sed -i "s/$ancienneURL/$nouvelleURL/g" htdocs/app-ecombox-ecombox-module.js.map
        }
        else {
           #Write-Host "L'ancienne URL $ancienneURL est identique à la nouvelle $nouvelleURL."
           Write-Output "" >> $pathlog\ecombox.log
           Write-Output "L'ancienne URL $ancienneURL est identique à la nouvelle $nouvelleURL." >> $pathlog\ecombox.log
           }      
      
     Write-Output "" >> $pathlog\ecombox.log
     Write-Output "Configuration d'e-comBox réalisée." >> $pathlog\ecombox.log
     Write-Host ""
     Write-Host "Configuration d'e-comBox réalisée. L'application va être lancée dans votre navigateur."
}


Function nettoyageImages
{
      Write-host ""      
      Write-host "Suppression des images si elles ne sont associées à aucun site"
      Write-host ""
      write-Output "" >> $pathlog\ecombox.log
      Write-Output "Suppression des images si elles ne sont associées à aucun site" >> $pathlog\ecombox.log
      docker rmi $(docker images -q) 2>$null *>> $pathlog\ecombox.log

      # Suppression des éventuelles images dangling
      if (docker images -q -f dangling=true) {
          docker rmi $(docker images -q -f dangling=true) 2>$null *>> $pathlog\ecombox.log
      }
}


Function creerPass
{

Param (
  [string]$type)

if ($type -eq "ecb") {
   $fileName = "authName.txt" 
   $filePass = "authPass.txt"
   }
   else {
      $fileName = "portainerName.txt" 
      $filePass = "portainerPass.txt"
      }
	
if ($type -eq "ecb") {
   Do {

      $Cred1 = Get-Credential -Message "Saisissez le nom d'utilisateur et le mot de passe que vous voulez utiliser pour accéder à l'interface d'e-comBox."
      $Cred2 = Get-Credential -UserName $Cred1.GetNetworkCredential().UserName -Message "Saisissez de nouveau le mot de passe."
      if ($Cred1.GetNetworkCredential().Password -cne $Cred2.GetNetworkCredential().Password) {$retour = popupInformation -titre "Mot de passe non valide" -message "Les mots de passe ne correspondent pas. Veuillez recommencer !"}      
          
   } Until (($Cred1.GetNetworkCredential().Password -ceq $Cred2.GetNetworkCredential().Password))

   $Cred = $Cred1
   }

   else {
        Do {

           $Cred1 = Get-Credential -UserName "admin" -Message "Saisissez le mot de passe que vous avez attribué au compte `"admin`" de Portainer."
           $Cred2 = Get-Credential -UserName "admin" -Message "Saisissez de nouveau le mot de passe."
           if ($Cred1.GetNetworkCredential().Password -cne $Cred2.GetNetworkCredential().Password) {$retour = popupInformation -titre "Mot de passe non valide" -message "Les mots de passe ne correspondent pas. Veuillez recommencer !"}      
        
        } Until (($Cred1.GetNetworkCredential().Password -ceq $Cred2.GetNetworkCredential().Password))
      }

if ($cred) {
   
   $password = $cred.GetNetworkCredential().Password
   if ($password -eq "") {
      if ($type -eq "ecb") {
          $retour = popupExclamation -titre "Mot de passe non valide" -message "Le mot de passe est vide. L'authentification à l'e-comBox se fera sans mot de passe."      
          }
          else {
             $retour = popupExclamation -titre "Mot de passe non valide" -message "Le mot de passe est vide. La synchonisation du mot de passe de Portainer ne peut pas s'effectuer. `n`nVeuillez recommencer l'opération."      
             }
      If (Test-Path "$pathconf\$fileName") { 
          Remove-Item "$pathconf\$fileName"
          }
      If (Test-Path "$pathconf\$filePass") { 
          Remove-Item "$pathconf\$filePass"
          }
      }
      else {
        $Cred.UserName | Out-File $pathconf\$fileName
        $Cred.Password | ConvertFrom-SecureString | Out-File $pathconf\$filePass
        if ($type -eq "ecb") {
          $retour = popupInformation -titre "Authentification valide" -message "Le nom d'utilisateur et le mot de passe ont été créés. `n`Merci de vider le cache de votre navigateur afin que la nouvelle authentification à l'e-comBox soit prise en compte."      
          }
              
        Write-Output "Le nom d'utilisateur et le mot de passe ont été créés dans les fichiers $fileName et $filePass."  >> $pathlog\ecombox.log
        Write-Output ""  >> $pathlog\ecombox.log              
        }
    }
    else {
        Write-Output "L'action a été annulée."  >> $pathlog\ecombox.log
        Write-Output ""  >> $pathlog\ecombox.log
    }
}

Function creerAuth
{
If ((Test-Path "$pathconf\authName.txt") -and (Test-Path "$pathconf\authPass.txt")) {

   $name = Get-Content $pathconf\authName.txt
   $securePass = Get-Content $pathconf\authPass.txt | ConvertTo-SecureString
   $auth = New-Object System.Management.Automation.PSCredential -ArgumentList $name,$securePass
   $pass = $auth.GetNetworkCredential().Password

   # Création du .htaccess

   $test_htaccess = docker exec e-combox bash -c 'cat htdocs/.htaccess' 2>&1
      if ($test_htaccess -eq $null)
      {
       docker exec e-combox bash -c 'rm htdocs/.htaccess' *>> $pathlog\ecombox.log
      }
   
   docker exec e-combox bash -c 'touch htdocs/.htaccess' *>> $pathlog\ecombox.log       
   docker exec e-combox bash -c 'echo AuthType Basic > htdocs/.htaccess' *>> $pathlog\ecombox.log
   docker exec e-combox bash -c 'echo -e AuthName \"E-COMBOX\" >> htdocs/.htaccess' *>> $pathlog\ecombox.log
   docker exec e-combox bash -c 'echo -e AuthBasicProvider file >> htdocs/.htaccess' *>> $pathlog\ecombox.log
   docker exec e-combox bash -c 'echo -e AuthUserFile /etc/ecombox-conf/.auth >> htdocs/.htaccess' *>> $pathlog\ecombox.log
   docker exec e-combox bash -c 'echo -e Require valid-user >> htdocs/.htaccess' *>> $pathlog\ecombox.log
   # docker exec e-combox bash -c 'cat htdocs/.htaccess'

   $test_auth = docker exec e-combox bash -c 'cat /etc/ecombox-conf/.auth'
   if ($test_auth -eq "null")
      {
       docker exec e-combox bash -c 'rm /etc/ecombox-conf/.auth' *>> $pathlog\ecombox.log
      }
   
   docker exec e-combox htpasswd -cBb /etc/ecombox-conf/.auth $name $pass *>> $pathlog\ecombox.log
   }
}

Function supprimerAuth {

Param (
  [string]$type
)

if ($type -eq "ecb") {
   $fileName = "authName.txt" 
   $filePass = "authPass.txt"
   }
   else {
      $fileName = "portainerName.txt" 
      $filePass = "portainerPass.txt"
      }

# Pour l'instant pas de test sur le type car seul le type "ecb" est concerné
If ((Test-Path "$pathconf\$fileName") -or (Test-Path "$pathconf\$filePass"))  {
       if (Test-Path "$pathconf\$fileName") { 
          Remove-Item "$pathconf\$fileName" -Force
          Write-Output "Le fichier $pathconf\$fileName a été supprimé."  >> $pathlog\ecombox.log
          Write-Output ""  >> $pathlog\ecombox.log
          #write-host Le fichier $pathconf\$fileName a été supprimé.
          }
       if (Test-Path "$pathconf\$filePass") { 
          Remove-Item "$pathconf\$filePass" -Force
          Write-Output "Le fichier $pathconf\$fileName a été supprimé."  >> $pathlog\ecombox.log
          Write-Output ""  >> $pathlog\ecombox.log
          #write-host Le fichier $pathconf\$filePass a été supprimé.
          }
       docker exec e-combox bash -c 'rm htdocs/.htaccess' *>> $pathlog\ecombox.log
       docker exec e-combox bash -c 'rm /etc/ecombox-conf/.auth' *>> $pathlog\ecombox.log
       Write-Output "Les fichiers .htaccess et .auth ont été supprimés."  >> $pathlog\ecombox.log
       Write-Output ""  >> $pathlog\ecombox.log
     }
}      


Function supprimerAuthAvecPopup {

Param (
  [string]$type
)

if ($type -eq "ecb") {
   $fileName = "authName.txt" 
   $filePass = "authPass.txt"
   }
   else {
      $fileName = "portainerName.txt" 
      $filePass = "portainerPass.txt"
      }

# Pour l'instant pas de test sur le type car seul le type "ecb" est concerné
If ((Test-Path "$pathconf\$fileName") -or (Test-Path "$pathconf\$filePass"))  {
    $retour = popupStop -titre "Suppression de l'authentification pour accéder à l'e-comBox" -message "Êtes-vous certain de vouloir supprimer l'authentification pour accéder à l'e-comBox ?"      
     
    if ($retour -eq 6) 
       {
       if (Test-Path "$pathconf\$fileName") { 
          Remove-Item "$pathconf\$fileName" -Force
          }
       if (Test-Path "$pathconf\$filePass") { 
          Remove-Item "$pathconf\$filePass" -Force
          }
       docker exec e-combox bash -c 'rm htdocs/.htaccess' *>> $pathlog\ecombox.log
       docker exec e-combox bash -c 'rm /etc/ecombox-conf/.auth' *>> $pathlog\ecombox.log
     
       $retour = popupExclamation -titre "Suppression de l'authentification pour accéder à l'e-comBox" -message "L'accès à l'e-comBox se fait maintenant sans authentification. `n`Si vous rencontrez un problème, videz le cache de votre navigateur."
       Write-Output "Les fichiers $pathconf\$fileName et $pathconf\$filePass contenant les nom d'utilisateur et le mot de passe ont été supprimés"  >> $pathlog\ecombox.log
       Write-Output ""  >> $pathlog\ecombox.log
       Write-Output "Les fichiers .htaccess et .auth ont été supprimés."  >> $pathlog\ecombox.log
       Write-Output ""  >> $pathlog\ecombox.log 
       }
       else {
           Write-Output "L'utilisateur a renoncé à la suppression de l'authentification."  >> $pathlog\ecombox.log
           Write-Output ""  >> $pathlog\ecombox.log
          }
    }
    else {
       $retour = popupExclamation -titre "Suppression de l'authentification pour accéder à l'e-comBox" -message "Aucune authentification pour accéder à l'interface de l'e-comBox n'est actuellement configurée."      
       Write-Output "Aucune authentification configurée : pas de proposition de suppression."  >> $pathlog\ecombox.log
       Write-Output ""  >> $pathlog\ecombox.log
    }
}      

Function synchroPassPortainer
{
If ((Test-Path "$pathconf\portainerName.txt") -and (Test-Path "$pathconf\portainerPass.txt")) {
   $nom = Get-Content $pathconf\portainerName.txt
   $securePass = Get-Content $pathconf\portainerPass.txt | ConvertTo-SecureString
   $auth = New-Object System.Management.Automation.PSCredential -ArgumentList $nom,$securePass
   $pass = $auth.GetNetworkCredential().Password

   $ancienPass = docker exec e-combox cat htdocs/main.js | Select-String "Password:"
   $ancienPass = $ancienPass -replace ",",""

   docker exec e-combox sed -i "s/$ancienPass/Password: '$pass'/g" htdocs/main.js
   docker exec e-combox sed -i "s/$ancienPass/Password: '$pass'/g" htdocs/main.js.map
   }
}

Function supprimerSites
{

$reponse = popupStop -titre "Suppression des sites" -message "il va être procédé à la suppression de vos sites, les données ne pourront pas être récupérées. `n`nLe mot de passe de Portainer va être réinitialisé et l'éventuelle authentification à l'interface va être supprimée. `n`nLe processus peut être assez long, veuillez patienter... `n`nCliquez sur Oui pour confirmer ou sur Non pour arrêter le processus."

   if ($reponse -eq 6) 
     {
       Write-Output "Suppression de l'authentification :" >> $pathlog\ecombox.log
       Write-Host "Suppression de l'authentification"
       supprimerAuth -type "ecb"
       supprimerAuth -type "p"

       if (docker ps -q) {       
           Write-Output "Arrêt des conteneurs :" >> $pathlog\ecombox.log
           docker stop $(docker ps -q) *>> $pathlog\ecombox.log          
           Write-Output "" >> $pathlog\ecombox.log
        }
      
      #docker system prune -a --volumes -f --filter "images" *>> $pathlog\ecombox.log
           
      docker rm -fv $(docker ps -a --format '{{.Names}}' |  Select-String -Pattern e-combox, docker-gen, nginx, portainer, prestashop, woocommerce, humhub, kanboard, mautic, blog, odoo, suitecrm)  *>> $pathlog\ecombox.log
      docker volume prune -f *>> $pathlog\ecombox.log
      docker image prune -af *>> $pathlog\ecombox.log
      docker network prune -f *>> $pathlog\ecombox.log

      Write-Output "" >> $pathlog\ecombox.log
      Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Suppression de tous les objets réalisée :" >> $pathlog\ecombox.log
      Write-host ""
      Write-Host "La suppression des sites est terminée, le système va procéder à la réinitialisation de l'application e-comBox."      

      # Réinitialisation de l'appli
      Write-Output "" >> $pathlog\ecombox.log
      Write-Output "$$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Réinitialisation de l'application en cours." >> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log
      Write-host ""
      Write-Host "Réinitialisation de l'application en cours..."
      Write-Host ""
      Start-Process "$pathscripts\lanceScriptPS_initialisationApplication.bat" -wait -NoNewWindow
      Write-Output "" >> $pathlog\ecombox.log
      Write-Output "$(Get-Date) - Réinitialisation de l'application réalisée." >> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log       
     }   
     else {
          #return $supprimeSite = $false
          Write-Output "" >> $pathlog\ecombox.log
          Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Suppression de tous les objets annulée." >> $pathlog\ecombox.log
          Write-host ""
          Write-Host "La suppression des sites a été annulée, ce n'est pas la peine de réinitialiser l'application."  
          }              
}

Function recupTailleDisque
{
     # Récupération de la taille du disque du PC en Ko
     $taille_disque=(Get-WmiObject -Class Win32_LogicalDisk -Filter "DeviceID='C:'").Size
     #write-host $taille_disque
     return $taille_disque
}

Function recupEspaceLibreDisque
{
     # Récupération de la taille libre du disk du PC en Ko
     $espace_libre_disque=(Get-WmiObject -Class Win32_LogicalDisk -Filter "DeviceID='C:'").FreeSpace
     #Write-Host $espace_libre_disque
     return $espace_libre_disque
}

Function calculPourcentageLibreDisque
{
      # Pourcentage de libre
      $taille_disque = recupTailleDisque
      $espace_libre_disque = recupEspaceLibreDisque
      # $espace_libre_disque_pourcent=($taille_disque-$espace_libre_disque)*100/$taille_disque
      $espace_libre_disque_pourcent="{0:p2}" -f (($espace_libre_disque)/$taille_disque)
      #write-host $espace_libre_disque_pourcent
      return $espace_libre_disque_pourcent
}

Function recupTailleVHD
{
     # Récupération de la taille du disk VHD en Mo
     $file = "$env:LOCALAPPDATA\Docker\wsl\data\ext4.vhdx"

     $taille_vdisk=
@"
select vdisk file=$file
attach vdisk readonly
detail vdisk
detach vdisk
exit
"@ | diskpart | Select-String "Taille physique"

     $taille_vdisk=$taille_vdisk.Line.Split(":")[1]

     # Quand le VHD ne prend pas beaucoup d'espace la taille est en MO
     if ($taille_vdisk -match "M") {
         $taille_vdisk=($taille_vdisk -replace '[a-zA-Z]+','').trim()
         $taille_vhd=[decimal]$taille_vdisk
         
     } else {
             $taille_vdisk=(($taille_vdisk -replace '[a-zA-Z]+','').trim())
             $taille_vhd=[decimal]$taille_vdisk
             $taille_vhd=$taille_vhd*1024             
            }

     return $taille_vhd
}

Function calculTailleOccupeVHD
{
     # Récupération de la taille réellement occupé en Mo
     $taille_occupe_vhd=docker system df --format '{{.Size}}'
     # Taille des images en MB
     $taille_image=($taille_occupe_vhd.GetValue(0)).trimend("GB")
     $taille_image_MB=[decimal]$taille_image*1024
     
     # Taille des conteneurs en MB
     $taille_conteneur=($taille_occupe_vhd.GetValue(1)).trimend("MB")
     $taille_conteneur_MB=[decimal]$taille_conteneur
     
     # Taille des volumes en MB
     $taille_volume=($taille_occupe_vhd.GetValue(2)).trimend("GB")
     $taille_volume_MB=[decimal]$taille_volume*1024
     
     # Taille totale de l'espace réellement occupé sur le VHD
     $taille_occupe_vhd=$taille_image_MB+$taille_conteneur_MB+$taille_volume_MB
     
     return $taille_occupe_vhd
}

Function calculEspaceAlibererMo
{
     $taille_vhd = recupTailleVHD
     $taille_occupe_vhd = calculTailleOccupeVHD

     # Espace qui peut être libéré en Mo
     $espace_libre_vhd=$taille_vhd-$taille_occupe_vhd
        
     return $espace_libre_vhd
}

Function calculEspaceAlibererPourcent
{
     $taille_vhd = recupTailleVHD
     $taille_occupe_vhd = calculTailleOccupeVHD

     # Espace qui peut être libéré en Mo
     $espace_libre_vhd=$taille_vhd-$taille_occupe_vhd
     
     # Pourcentage d'espace non occupé
     #$espace_libre_vhd_pourcent=$espace_libre_vhd*100/$taille_vhd
     $espace_libre_vhd_pourcent="{0:p2}" -f ($espace_libre_vhd/$taille_vhd)
     
     return $espace_libre_vhd_pourcent
}


Function reductionVHD {

    $file = "$env:LOCALAPPDATA\Docker\wsl\data\ext4.vhdx"

    wsl -e sudo fstrim /
    wsl --shutdown

@"
select vdisk file=$file
attach vdisk readonly
compact vdisk
detach vdisk
exit
"@ | diskpart

    # Relance de Docker
    demarreDocker

}



Function optimisationEspace {

   Write-Host "Attention à ne pas redémarrer Docker. Le système le fera lorsque les opérations seront terminées."
  
   $taille_vhd_avant = recupTailleVHD
     
   # Suppression des images non utilisées
   nettoyageImage

   # Optimisation du VHDX
   reductionVHD

   $taille_vhd_après = recupTailleVHD

   $espace_recupere_go = ($taille_vhd_avant - $taille_vhd_après)/1KB
   $espace_recupere_go = "{0:N2}" -f $espace_recupere_go

   Write-Output "L'espace disque récupéré est de $espace_recupere_go Go" >> $pathlog\ecombox.log
   Write-Output "" >> $pathlog\ecombox.log

   $reduc = popupExclamation -titre "Réduction effective" -message "Vous avez récupéré $espace_recupere_go Go."

}


Function verifEspace {

# Alertes en cas d'espace disque à optimiser

# Alerte en cas d'espace disque virtuel si au moins 60 % peut être libéré

Write-Output "`n`nEspace non occupé du VHD." >> $pathlog\ecombox.log

$taille_vhd = recupTailleVHD
$taille_vhd_go = $taille_vhd/1KB
Write-Output "`nLa taille du VHD est $taille_vhd Mo soit $taille_vhd_go Go." >> $pathlog\ecombox.log

$taille_occupe_vhd = calculTailleOccupeVHD
$taille_occupe_vhd_go = $taille_occupe_vhd/1KB
Write-Output "L'espace occupé du VHD est $taille_occupe_vhd Mo soit $taille_occupe_vhd_go Go." >> $pathlog\ecombox.log

$espace_libre_vhd = $taille_vhd - $taille_occupe_vhd
$espace_libre_vhd_go = $espace_libre_vhd/1KB
$espace_libre_vhd_go = "{0:N2}" -f ($espace_libre_vhd_go)

Write-Output "L'espace non occupé du VHD est $espace_libre_vhd Mo soit $espace_libre_vhd_go Go." >> $pathlog\ecombox.log

$taux_espace_libre_vhd = $espace_libre_vhd/$taille_vhd
$taux_espace_libre_vhd_pourcent="{0:p4}" -f ($taux_espace_libre_vhd)
Write-Output "Le taux d'espace non occupé du VHD est de $taux_espace_libre_vhd_pourcent." >> $pathlog\ecombox.log

# On ne réduit pas si ce n'est pas vraiment utile
if ($taille_vhd -ge 10 -and $espace_libre_vhd_go -ge 4 -and $taux_espace_libre_vhd -ge 0.6) {

   Write-Output "" >> $pathlog\ecombox.log
   Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') -  Optimisation de l'espace" >> $pathlog\ecombox.log
   Write-Output "" >> $pathlog\ecombox.log
   Write-Output "L'espace disque virtuel peut être libéré de $espace_libre_vhd_pourcent" >> $pathlog\ecombox.log
   Write-Output "" >> $pathlog\ecombox.log

   $reduc = popupExclamation -titre "Réduction de l'espace disque utilisé par les sites" -message "Vous disposez de $taux_espace_libre_vhd_pourcent d'espace non utilisé par les sites. `n`nLe système va procéder à l'optimisation de l'espace occupé par l'e-comBox avant de lancer l'application.`n`nVous ne devez pas redémarrer Docker lorsque cela vous le sera demandé. Le système procèdera automatiquement au redémarrage à la fin des opérations." 
   
   $optimisation = optimisationEspace      

}

# Alerte en cas d'espace disque plein à 80 % ou d'espace libre est inférieure à 40 Go

Write-Output "`n`nEspace libre DISQUE." >> $pathlog\ecombox.log

$taille_disque = recupTailleDisque
Write-Output "`nLa taille du disque est de $taille_disque Ko." >> $pathlog\ecombox.log 

$taille_libre_disque = recupEspaceLibreDisque
$taille_libre_disque_go =  ($taille_libre_disque)/1GB
$taille_libre_disque_go = "{0:N2}" -f  $taille_libre_disque_go
Write-Output "L'espace libre du disque est de $taille_libre_disque Ko soit $taille_libre_disque_go Go ." >> $pathlog\ecombox.log


$taux_espace_libre_disque = $taille_libre_disque/$taille_disque
$taux_espace_libre_disque_pourcent="{0:p4}" -f ($taux_espace_libre_disque)
Write-Output "Le taux d'espace libre du disque est de $taux_espace_libre_disque_pourcent." >> $pathlog\ecombox.log

$espace_libre_disque_pourcent="{0:p2}" -f ($taux_espace_libre_disque)

if (($taux_espace_libre_disque -le 0.2) -or ($taille_libre_disque -le 40)) {

   Write-Output "" >> $pathlog\ecombox.log
   Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') -  Optimisation de l'espace" >> $pathlog\ecombox.log
   Write-Output "" >> $pathlog\ecombox.log
   Write-Output "Il reste $espace_libre_disque_pourcent de libre sur le disque ce qui représente $taille_libre_disque_go Go" >> $pathlog\ecombox.log
   Write-Output "" >> $pathlog\ecombox.log

   $reduc = popupExclamation -titre "Réduction de l'espace disque utilisé par les sites" -message "Votre espace libre est égal $espace_libre_disque_pourcent ce qui représente $taille_libre_disque_Go Go. `n`nLe système va tenter d'optimiser l'espace occupé par l'e-comBox avant de lancer l'application. `n`n Merci de ne pas redémarrer Docker. Le système procèdera au redémarrage à la fin des opérations" 
   
   $optimisation = optimisationEspace
}

}


Function lanceURL
{
      $config = valeurConfig

      # Récupération du l'adresse IP dans le fichier de paramètre      
      $ip_host = $config.ADRESSE_IP
      
      # Récupération du port (8888 par défaut) dans le fichier paramètre
      $ecb_port = $config.PORT_ECB

      # Construction de l'URL
      $url_acces_ecb = -join("$ip_host",":","$ecb_port")

      # Détection du navigateur par défaut et lancement de l'URL
      New-PSDrive -Name HKCU -PSProvider Registry -Root HKEY_CURRENT_USER 2>> $null
      $Browser=Get-ItemProperty HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.html\UserChoice | Select-Object -ExpandProperty ProgId
      Switch ($Browser) {
               'ChromeHTML' { Write-Output "Le navigateur par défaut est Google Chrome" >> $pathlog\ecombox.log
                              [System.Diagnostics.Process]::Start("chrome.exe", "--incognito $url_acces_ecb") }

               'FirefoxHTML-308046B0AF4A39CB' { Write-Output "Le navigateur par défaut est Mozilla FireFox" >> $pathlog\ecombox.log
                                                [System.Diagnostics.Process]::Start("firefox.exe","-private-window $url_acces_ecb")}

               'MSEdgeHTM' { Write-Output "Le navigateur par défaut est Microsoft Edge" >> $pathlog\ecombox.log
                            [system.Diagnostics.Process]::Start("msedge","-inPrivate $url_acces_ecb") }

                default { Write-Output "Le navigateur par défaut n'a pas pu être déterminé" >> $pathlog\ecombox.log
                          Start-Process "http://$url_acces_ecb/" *>> $pathlog\ecombox.log }
            }
      
      Write-Output "" >> $pathlog\ecombox.log
      Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - L'application e-comBox a été lancée dans le navigateur." >> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log
      popupInformation -titre "Lancement d'e-combox" -message "L'application e-comBox a été lancée dans le navigateur."
}
