﻿function verifPatch
{
    $version_correctif = Get-Content $pathconf\.version_correctif
    $version_correctif_local = $version_correctif[0].split("=")[1]

    Write-Output "`nla version local est $version_correctif_local." >> $pathlog\ecombox.log

    # Téléchargement de la version en cours sur gitlab
    Invoke-WebRequest -Uri "https://gitlab.com/e-combox/e-comBox_setupWin10pro/-/raw/$branche/.version_correctif?inline=false" -OutFile "$pathconf\.version_correctif_gitlab" >> $pathlog\ecombox.log

    $version_correctif = Get-Content $pathconf\.version_correctif_gitlab
    $version_correctif_gitlab = $version_correctif[0].split("=")[1]

    Write-Output "La version distante est $version_correctif_gitlab." >> $pathlog\ecombox.log
     
    if ($version_correctif_local -ne $version_correctif_gitlab) {
        Write-Output "`nLe correctif $version_correctif_gitlab est disponible." >> $pathlog\ecombox.log

        $patch = popupExclamation -titre "Correctif à installer" -message "Un correctif pour la gestion de l'e-comBox est disponible.`n`nIl sera installé avant le lancement de l'application."  
   
        # Téléchargement du patch sur gitlab
        $nom_correctif = -join("installCorrectif","_","$version_correctif_gitlab",".ps1")
        $url = -join("https://gitlab.com/e-combox/e-comBox_setupWin10pro/-/raw/$branche/$nom_correctif","?inline=false")
    
        Invoke-WebRequest -Uri "$url" -OutFile "$pathscripts\installCorrectif.ps1" >> $pathlog\ecombox.log
   
        $correctif=patchSetup
     
        #Suppression du patch
        rm 'installCorrectif.ps1'

    } else {Write-Output "`nAucun correctif n'est disponible"}
}