﻿# Réinitialisation de l'environnement

# Déclaration des chemins (logs, scripts et bibliothèque des fonctions)
$pathlog="$env:USERPROFILE\.docker\logEcombox"
$pathconf="$env:USERPROFILE\.docker\confEcombox"
$pathscripts="C:\Program Files\e-comBox\scripts\"
#. "$env:USERPROFILE\e-comBox_setupWin10pro\fonctions.ps1"
. "$pathscripts\fonctions.ps1"

Write-host ""
Write-host "============================================================================="
Write-host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Réinitialisation de l'environnement"
Write-host "============================================================================="
Write-host ""

Write-Output "" >> $pathlog\ecombox.log
Write-Output "=============================================================================" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Réinitialisation de l'environnement" >> $pathlog\ecombox.log
Write-Output "=============================================================================" >> $pathlog\ecombox.log


# Ajout des informations importantes sur le matériel et sur son utilisation par Docker
ajoutInfosLog

# Vérification de la connexion à Internet
testConnectInternet

# Vérification que Docker fonctionne correctement sinon on le redémarre
verifDocker
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin du processus de vérification de Docker." >> $pathlog\ecombox.log

# Ajout de infos sur le démarrage de Docker et WSL2 dans les log
Write-Output "" >> $pathlog\ecombox.log
Write-Output "Docker est-il bien démarré sur la version 2 de WSL2" >> $pathlog\ecombox.log
wsl -l -v 2>&1 >> $pathlog\ecombox.log

# Création éventuelle du réseau 192.168.97.0/24 utilisé pour tous les conteneurs de l'appli
Write-Output "" >> $pathlog\ecombox.log
Write-Output "Création du réseau des sites" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

if ((docker network ls) | Select-String bridge_e-combox) {
   Write-Output "Le réseau des sites existe déjà." >> $pathlog\ecombox.log
   Write-Output "" >> $pathlog\ecombox.log
   }
else {
   Write-Output "Le réseau des sites 192.168.97.0/24 n'existe pas, il sera créé :" >> $pathlog\ecombox.log
   Write-Output "" >> $pathlog\ecombox.log
   docker network create --subnet 192.168.97.0/24 --gateway=192.168.97.1 bridge_e-combox *>> $pathlog\ecombox.log
}

# Récup automatique d'une adresse IP valide
$docker_ip_host = recupIPhost

# Mise à jour du fichier de paramètres
$ligne = Get-Content $pathconf\param.conf | Select-String ADRESSE_IP
(Get-Content -Path $pathconf\param.conf) | ForEach-Object {$_ -replace "$ligne", "ADRESSE_IP=$docker_ip_host"} | Set-Content -Path $pathconf\param.conf

# Vérification de la configuration du Proxy sur Docker avec popup
$codeRetour = configProxyDockerAvecPopup

# Détection et configuration d'un éventuel proxy pour Git
configProxyGit

# Suppression de l'application
deleteApplication

# Récupération de portainer 
recupPortainer
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin du processus d'installation de Portainer." >> $pathlog\ecombox.log

# Configuration de L'URL et du port dans la config de Portainer
configPortainer 
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Fin du processus de configuration de Portainer." >> $pathlog\ecombox.log
Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Fin du processus de configuration de Portainer."

# Démarrage de Portainer et verification de son bon fonctionnement
startPortainer
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Fin du processus de démarrage de Portainer." >> $pathlog\ecombox.log
Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Fin du processus de démarrage de Portainer."

verifDocker
verifPortainer
verifPortainerApresVerifDocker

# Récupération, configuration et démarrage du reverse proxy
recupReverseProxy
configReverseProxy
startReverseProxy
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Fin du processus de démarrage du Reverse Proxy." >> $pathlog\ecombox.log
Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Fin du processus de démarrage du Reverse Proxy."

verifReverseProxy

# Nettoyage des anciennes images si elles ne sont associées à aucun site
nettoyageImages
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin processus de nettoyage des images." >> $pathlog\ecombox.log
Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin processus de nettoyage des images."


# Démarrage de l'application
$retour = popupInformation -titre "Reconfiguration et lancement de l'application" -message "La reconfiguration de l'application et son lancement peuvent prendre quelques minutes. Merci de patienter !"      
          
startApplication
configApplication
synchroPassPortainer
creerAuth
lanceURL

Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Fin processus du démarrage et du lancement de l'application." >> $pathlog\ecombox.log
